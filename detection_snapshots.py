import argparse
import datetime
import subprocess
from time import time
from datetime import datetime

import cv2

import os

# All time in seconds

STAMP_COLOR = (0, 0, 255)
VIDEO_DEVICE_ID = 0
PERIOD_BETWEEN_PROCESSING = 4
DETECTION_END_TIME_PERIOD = 60
REUPLOAD_PERIOD = 7200
SNAPSHOT_PERIODS = (0, 10, 20, 60, 120, 600)
DROPBOX_SCRIPT = '/home/pi/sb/Dropbox-Uploader/dropbox_uploader.sh'

upper_body_cascade = cv2.CascadeClassifier(
    cv2.data.haarcascades + "haarcascade_upperbody.xml")

def do_process(last_time_stamp):
    return (time() - last_time_stamp) > PERIOD_BETWEEN_PROCESSING

def detect(grey_frame):
    return upper_body_cascade.detectMultiScale(
                grey_frame, 1.1, 5)

def take_snapshot(counter, last_time):
    index = len(SNAPSHOT_PERIODS) - 1
    if counter < index:
        index = counter
    return time() - last_time > SNAPSHOT_PERIODS[index]


def upload(file_path, date, timestamp, extension, reupload_list):
    upload_path = os.path.join("images", date, timestamp + extension)
    upload = subprocess.run(
        [DROPBOX_SCRIPT, "-s", "upload", file_path, upload_path], capture_output=True, text=True)
    print(upload.args)
    print(upload.returncode)
    print(upload.stdout)
    if upload.returncode != 0:
        reupload_list.append(file_path)


def main(directory, show_window):
    last_process_time = 0
    face_detection_counter = 0
    snapshot_counter = 0
    detection_ongoing = False
    last_snapshot_time = 0
    last_detection_time = 0
    reupload_list = []
    last_reupload = 0
    video_capture = cv2.VideoCapture(VIDEO_DEVICE_ID)
    if not video_capture:
        print(f'No video capture device for id {VIDEO_DEVICE_ID}')
        exit(1)
    try:
        while True:
            _, frame = video_capture.read()
            # Crop frame
            frame = frame[110:320, 220:440]
            height, width = frame.shape[:2]

            if show_window:
                cv2.imshow('Frame', frame)
                if cv2.waitKey(1) == ord('q'):
                    break

            if not do_process(last_process_time):
                continue
            last_process_time = time()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            upper_body_locations = detect(gray)

            if len(upper_body_locations) > 0:
                face_detection_counter += 1
                detection_ongoing = True
                last_detection_time = time()
                print(f'DETECTED #{face_detection_counter}')
                # for (x, y, w, h) in upper_body_locations:
                #     cv2.rectangle(frame, (x, y), (x + w, y + h),
                #                   (0, 0, 255), 2)
                if take_snapshot(snapshot_counter, last_snapshot_time):
                    date_time = datetime.now()
                    file_path_date = date_time.strftime("%Y-%m-%d")
                    file_path_time = date_time.strftime("%H-%M-%S")
                    cv2.putText(frame, date_time.strftime("%d/%m/%Y"), (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                                STAMP_COLOR, 2, 2)
                    cv2.putText(frame, date_time.strftime("%H:%M:%S"), (width - 80, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                                STAMP_COLOR, 2, 2)
                    extension = ".jpg"
                    file_name = file_path_date + "_" + file_path_time + extension
                    file_path = os.path.join(directory, file_name)
                    snapshot_counter += 1
                    last_snapshot_time = time()
                    print(
                        f'file_name: {file_path}, snapshot counter {snapshot_counter}')
                    cv2.imwrite(file_path, frame)
                    upload(file_path, file_path_date, file_path_time,
                           extension, reupload_list)

            elif detection_ongoing:
                if (time() - last_detection_time) > DETECTION_END_TIME_PERIOD:
                    print('Stopping ongoing detection')
                    detection_ongoing = False
                    snapshot_counter = 0

    except KeyboardInterrupt:
        print('Stopping script with keyboard interrupt')
    finally:
        # Release handle to the camera
        video_capture.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Face Detection Script')
    parser.add_argument('snapshot_dir', metavar='snapshot_dir',
                        type=str, help='directory for saving snapshots')
    parser.add_argument('-w', '--window_show', dest='window',
                        action='store_true', help='show video window')
    parser.add_argument('-u', '--upload_script',
                        dest='upload_script', help='Path to upload script')
    parser.set_defaults(window=False)
    args = parser.parse_args()

    directory = args.snapshot_dir

    if args.upload_script:
        DROPBOX_SCRIPT = args.upload_script
        print(f'Using dropbox uploader script at location {DROPBOX_SCRIPT}')

    show_window = args.window
    if show_window:
        print('Showing video window')

    if not os.path.isdir(directory):
        print(f'Given snapshot output path "{directory}" is not a directory')
        exit(1)

    print(f'Using directory "{directory}" for saving images.')

    main(directory, show_window)
