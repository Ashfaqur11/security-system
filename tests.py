import unittest
import detection_snapshots
import cv2


class Test(unittest.TestCase):
    def test_detect_body(self):
        img = cv2.imread('test_pic.jpg', 0)
        upper_body_locations = detection_snapshots.detect(img)
        self.assertTrue(len(upper_body_locations) > 0)


if __name__ == "__main__":
    unittest.main()